#ifndef BUSQUEDA_H
#define BUSQUEDA_H

#include <iostream>
using namespace std;


class Busqueda {


    private:
     
    public:
        Busqueda();

        int Hash(int N, int K);
        void Prueba_lineal(int K,int n,int V[],int direccion);
        void Prueba_cuadratica();
        void R_doble_direccion_hash();
        void Encadenamiento();
        void arreglo(int opc,int i, int arreglo[], int hash[], int **final);
        void imprimir(int arreglo[]);
        void buscar(int hash[]);
        void unir(int hash[],int n,int **final,int K);
        
};
#endif