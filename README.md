# guia9_u3


Para poder usar esta aplicación usted debe tener IDE con leguaje c++ como Geany o Visual Studio Code, instalado en su computador.
Primero debe descargar todos los elementos . Abra la IDE y y abra la carpeta recien descargada
en la consola puede ejecutar el programa al escribir "make".

En este programa usted puede crear un arreglo de 10 elementos y al guardar un elemento se le pedira una clave, que le facilitara la buqueda del valor ingresado :

        ./hash [L|C|D|E]  -> representa el metodo de solucion de colisiones.
         L = Reasignación Prueba Lineal.
         C = Reasignación Prueba Cuadrática .
         D = Reasignación Doble Dirección Hash .
         E = Encadenamiento.
                         
Despues en pantalla usted vera un menu :

        **********************************
        Ingresar dato__________________[1] <- En este caso se le pedira un valor y una clave
        Buscar dato____________________[2] <- Escribe la clave y da el valor 
        Salir__________________________[0] <- Sale del programa
        **********************************
 
 Eso es todo gracias por usar este programa
